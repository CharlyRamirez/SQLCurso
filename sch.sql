create table books(
	book_id integer unsigned primary key auto_increment,
    editor_id integer unsigned not null default 0,
    titulo varchar(50) not null,
    autor varchar(100) not null,
    descripcion text,
    precio decimal(5,2),
    copias int not null default 0
);

INSERT INTO books(editor_id, titulo, autor, descripcion, precio, copias) VALUES
    (1, 'Mastering MySQL', 'John Goodman', 'Clases de bases de datos usando MySQL', 10.50, 4),
    (2, 'Trigonometria avanzada', 'Pi Tagoras', 'Trigonometria desde sus origenes', 7.30, 2),
    (3, 'Advanced Statistics', 'Carl Gauss', 'De curvas y otras graficas', 23.60, 1),
    (4, 'Redes Avanzadas', 'Tim Bernes-Lee', 'Lo que viene siendo el Internet', 13.50, 4),
    (2, 'Curvas Parabolicas', 'Napoleon TNT', 'Historia de la parabola', 6.99, 10),
    (1, 'Ruby On (the) Road', 'A Miner', 'Un nuevo acercamiento a la programacion', 18.75, 4),
    (1, 'Estudios basicos de estudios', 'John Goodman', 'Clases de datos usando MySQL', 10.50 , 4),
    (4, 'Donde esta Y?', 'John Goodman', 'Clases de datos usando MySQL', 10.50, 4),
    (3, 'Quimica Avanzada', 'John White', 'Profitable studies on chemistry', 45.35, 1),
    (4, 'Graficas Matematicas', 'Rene Descartes', 'De donde viene el plano', 13.99, 7),
    (4, 'Numeros Importantes', 'Leonard Euler', 'De numeros que a veces nos sirven', 10, 3),
    (3, 'Modelado de conocimiento', 'Jack Friedman', 'Una vez adquirido, como se guarda el conocimiento', 29.99, 2),
    (3, 'Teoria de juegos', 'John Nash', 'A o B?', 12.55, 3),
    (1, 'Calculo de variables', 'Brian Kernhigan', 'Programacion mega basica', 9.50, 3),
    (5, 'Produccion de streaming', 'Juan Pablo Rojas', 'De la oficina ala pan', 23.49, 9),
    (5, 'ELearning', 'JFD & DvdH', 'Diseno y ejecucion de educacion online', 23.55, 4),
    (5, 'Pet Caring for Geeks', 'KC', 'Que tu perro aprenda a programar', 18.79, 3 ),
    (1, 'Algebra basica', 'Al Juarismi', 'Esto de encontrar X o Y, dependiendo', 13.50, 8);

create table editor(
    editor_id integer unsigned primary key auto_increment,
    nombre varchar(100) not null,
    pais varchar(20)
);

create table usuarios(
    usuarios_id integer unsigned primary key auto_increment,
    nombre varchar(100) not null,
    email varchar(100) not null unique
);

create table acciones(
    acciones_id integer unsigned primary key auto_increment,
    book_id integer unsigned not null,
    usuarios_id integer unsigned not null,
    tipo_accion enum('venta', 'prestamo', 'devolucion') not null,
    creado timestamp not null default current_timestamp
);

INSERT INTO usuarios(nombre, email) VALUES
    ('Ricardo', 'ricardo@hola.com'),
    ('Laura', 'laura@hola.com'),
    ('Jose', 'jose@hola.com'),
    ('Sofia', 'sofia@hola.com'),
    ('Fernanda', 'fernanda@hola.com'),
    ('Jose Guillermo', 'memo@hola.com'),
    ('Maria', 'maria@hola.com'),
    ('Susana', 'susana@hola.com'),
    ('Jorge', 'jorge@hola.com');

INSERT INTO editor(nombre, pais) VALUES
    ('OReilly', 'USA'),
    ('Santillana', 'Mexico'),
    ('MIT Edu', 'USA'),
    ('UTPC', 'Colombia'),
    ('Platzi', 'USA');